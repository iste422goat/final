package xml;

import java.io.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.*;
import java.util.*;
import edge.*;


public class XMLConvertFileParser{

private File parseFile;
private FileReader fr;
private BufferedReader br;
private String dbname = "";
private String name 			= "";
private String tableName ="";
ArrayList<EdgeTable> tables = new ArrayList<EdgeTable>();
ArrayList<EdgeField> fields = new ArrayList<EdgeField>();


public XMLConvertFileParser(File _parseFile) {
   this.parseFile = _parseFile;
}

public void getData(){
   try{
   DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
   DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
   Document doc = dBuilder.parse(this.parseFile);
   doc.getDocumentElement();
   dbname = doc.getDocumentElement().getNodeName();
   NodeList nList = doc.getElementsByTagName("table");
   
   for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);
            tableName = nNode.getNodeName();
            
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
               Element eElement = (Element) nNode;
               tableName = eElement.getAttribute("name");
               NodeList fieldNameList = eElement.getElementsByTagName("field");
                  
               for (int count = 0; count < fieldNameList.getLength(); count++) {	 
                  Node node1 = fieldNameList.item(count);
                  if (node1.getNodeType() == node1.ELEMENT_NODE) {
                     Element fieldNode = (Element) node1;
                     name = fieldNode.getAttribute("name"); 
                                         
                     EdgeField field = new EdgeField(name,true,temp);
                     fields.add(field);
                  }
               }
            }
            String comment = "";
            EdgeTable table = new EdgeTable(tableName,fields);
            tables.add(table);            
         }
   }
   //fix this?
   catch (Exception e) {
   e.printStackTrace();
   }


}

//Added
public EdgeTable[] getEdgeTables(){
   EdgeTable[] array = tables.toArray(new EdgeTable[tables.size()]);
   return array;
}
//Added
public EdgeField[] getEdgeFields(){
   EdgeField[] array = fields.toArray(new EdgeField[fields.size()]);
   return array;
}





}